import logging

class BasicSort:
    
    def __init__(self, arr, sortfn):
        logging.debug("Sorting class is initialized.")
        self.__arr = arr
        self.__sortfn = sortfn.lower()
        
        if self.__sortfn == "insertsort":
            self.insertsort()
        elif self.__sortfn == "bubblesort":
            self.bubblesort()
    
    
    #Insert Sort
    def insertsort(self):
        logging.debug("Initializing InsertSort Function.")
        n = len(self.__arr)
        for i in range(1, n):
            key = self.__arr[i]
            j = i - 1
            while j >=0 and self.__arr[j] > key:
                self.__arr[j+1] = self.__arr[j]
                j = j - 1
            self.__arr[j+1] = key
        logging.debug("Sorted Array after InsertSort : {}".format(self.__arr))
        logging.debug("InsertSort Function completed successfully.")
    
    #Bubble Sort
    def bubblesort(self):
        logging.debug("Initializing BubbleSort Function.")
        n = len(self.__arr)
        for j in range(1, n):
            for i in range(0, n - 1):
                if self.__arr[i] > self.__arr[i+1]:
                    temp = self.__arr[i]
                    self.__arr[i] = self.__arr[i+1]
                    self.__arr[i+1] = temp
        logging.debug("Sorted Array after BubbleSort : {}".format(self.__arr))
        logging.debug("BubbleSort Function completed successfully.")

    @property
    def sortedoutput(self):
        return self.__arr

    def __del__(self):
        logging.debug("Sorting Class terminated successfully.")